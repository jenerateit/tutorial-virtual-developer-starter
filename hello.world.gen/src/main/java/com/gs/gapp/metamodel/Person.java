package com.gs.gapp.metamodel;

public class Person {

    private final String name;
    
    private HairColor hairColor;
    
    
    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public HairColor getHairColor() {
		return hairColor;
	}

	public void setHairColor(HairColor hairColor) {
		this.hairColor = hairColor;
	}

	public static class HairColor {
    	private final String color;

		public HairColor(String color) {
			super();
			this.color = color;
		}

		public String getColor() {
			return color;
		}
    	
    	
    }
}