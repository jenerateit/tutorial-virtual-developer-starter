package com.gs.gapp.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.target.PersonTarget;
import com.gs.gapp.target.PersonTarget.HairColorWriter;
import com.gs.gapp.target.PersonTarget.PersonWriter;

import com.gs.gapp.metamodel.Person;
import com.gs.gapp.metamodel.Person.HairColor;

/**
 *
 */
public class GenerationGroupHelloWorld implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
        if (modelElement instanceof Person) {
            return PersonWriter.class;
        }
		
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		
		@SuppressWarnings({ "unchecked" })
		Class<? extends TargetI<?>> targetClazz = (Class<? extends TargetI<?>>) targetInstance.getClass();
		Class<? extends WriterI> writerClass = getWriterClass(modelElement, targetClazz);
		
		if (modelElement instanceof HairColor && targetInstance instanceof PersonTarget) {
			writerClass = HairColorWriter.class;
		}
		
		return writerClass;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
        result.add(PersonTarget.class);
		return result;
	}
}
