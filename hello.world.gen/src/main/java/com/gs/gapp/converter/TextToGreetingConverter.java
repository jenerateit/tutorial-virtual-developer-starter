package com.gs.gapp.converter;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.Person;
import com.gs.gapp.metamodel.Person.HairColor;

/**
 *
 */
public class TextToGreetingConverter extends MessageProviderModelConverter {
	

	
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		
		for (Object element : elements) {
			if (element instanceof String) {
				String name = (String) element;
				result.add(name);  // re-add the original element in order to give a subsequent converter the chance to handle it, too
                Person newElement = new Person(name);
                if (name.length() % 2 == 0) {
                	newElement.setHairColor(new HairColor("green"));
                } else {
                	newElement.setHairColor(new HairColor("brown"));
                }
                result.add(newElement);
			}
		}
		
		return result;
	}
}
